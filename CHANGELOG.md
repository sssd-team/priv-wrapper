CHANGELOG
=========

## Version 1.0.1 (released 2022-11-24)
* Fixed dclose() with RTLD_NEXT
* Fixed prctl() dlsym prototype

## Version 1.0.0 (released 2022-10-24)
* Initial release
